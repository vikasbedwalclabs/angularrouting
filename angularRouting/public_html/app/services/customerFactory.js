
(function() {
    /* Factory object and function created  */
    var customerFactory = function($http) {
        /* Array holds company data including CEO details also  */

        /*   Expose factory object */
        var factory = {};
        factory.getCustomers = function() {
            return $http.get('/customers');
        };
        /*  Search the customer for customer ID */
        factory.getCustomer = function(custId) {
            return $http.get('/customers/' + custId);
        };
        factory.getList = function($scope) {
            $scope.save = function($location) {
                $scope.customers.push({'company': $scope.company, 'ceo': $scope.ceo,
                    'headquarters': $scope.headquarters, 'noe': $scope.noe});
                $scope.company = '';
                $scope.ceo = '';
                $scope.headquarters = '';
                $scope.noe = '';
                {
                    return customers;
                }
                $location.path('/');
            };
        };
        factory.getDetail = function() {
            return $http.get('/detail');
        };
        factory.deleteCustomer = function(custId) {
            return $http.delete('/customers/' + custId);
        };
        return factory;
    };
    /* Just for minification purpose */
    customerFactory.$inject = ['$http'];
    /* Customer Factory Registered with module  */
    angular.module('myApp').factory('customerFactory', customerFactory);
}());
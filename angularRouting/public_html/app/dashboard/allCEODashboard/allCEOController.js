(function() {
          var allCEOController = function($scope, customerFactory) {
        $scope.customers=[];
              $scope.detail = null;
        $scope.ageTotal=0;
        $scope.totalType;
        function init() {
          /*  getDetail resides into customerFactory called  */
            customerFactory.getDetail()
                    .success(function(detail) {
                        $scope.detail = detail;
                        getAgeTotal();
                    })
                            .error(function(data,staus,headers,config){
                              //Handle Errors       
                            });
            customerFactory.getCustomers()
              .success(function(customers){
                      $scope.customers=customers;
                            });
        }
       /* Function to add ages of all CEO if incomeTotal>100000 then a particular row's color should be change  */
        function getAgeTotal(){
            var total=0;
            for(var i=0, len=$scope.detail.length; i<len; i++){
                total+= $scope.detail[i].income;
            }
            $scope.incomeTotal=total;
            $scope.totalType=($scope.incomeTotal>100000)? 'success' : 'danger';
        }
        init();
    };
    allCEOController.$inject=['$scope','customerFactory'];
    angular.module('myApp').controller('allCEOController',allCEOController);
}());
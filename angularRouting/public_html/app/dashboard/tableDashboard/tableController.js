/*  Controller  */
(function() {
    var tableCtrl = function($scope, $log, customerFactory, appSettings) {
        $scope.appSettings = appSettings;
        $scope.doSort = function(propName) {
            $scope.sortBy = propName;
            $scope.reverse = !$scope.reverse;
        };
        var customers = [];
        function init()
        {
            customerFactory.getCustomers()
                    .success(function(customers) {
                        $scope.customers = customers;
                    })
                    .error(function(data, status, header, config) {
                        $log.log('');
                        //handle header
                    });
        }
        init();
        /* Function to perform deletion opration on customer table */
        $scope.deleteCustomer = function(custId) {
            customerFactory.deleteCustomer(custId)
                    .success(function(status) {
                        if (status) {
                            for (var i = 0, len = $scope.customers.length; i < len; i++) {
                                if (($scope.customers[i].id) === custId) {
                                    $scope.customers.splice(i, 1);
                                    break;
                                }
                            }
                        }
                        else {
                            window.alert("Unable to delete Customer");
                        }
                    });
        };
    };
    tableCtrl.$inject = ['$scope', '$log', 'customerFactory', 'appSettings'];
    /* tableCtrl Registered with module  */
    angular.module('myApp').controller('tableCtrl', tableCtrl);
}());
 
/*Here using option 3 to use the controller which is little bit different from previous used controller's conventions */
(function() {
    var infoController = function($scope, $routeParams, customerFactory) {        
        var custId = $routeParams.custId;
        $scope.customer = null;
        function init()
        {
            customerFactory.getCustomer(custId)
                    .success(function(customer) {
                        $scope.customer = customer;
                    })
                    .error(function(data, status, header, config) {
                        //handle header
                    });
        }
        /*  init called which is defined in upper part  */
        init();
    };
    infoController.$inject = ['$scope', '$routeParams', 'customerFactory'];
    angular.module('myApp').controller('infoController', infoController);

}());
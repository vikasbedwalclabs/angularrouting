/* Using Multiple routes */
(function(window) {
    /*    Helper modules injected  */
    var app = angular.module("myApp", ['ngRoute','ngAnimate']);
    /* Configuring various routes   */
    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                    /* Default */
                    .when('/', {
                        controller: 'tableCtrl',
                        templateUrl: 'app/dashboard/tableDashboard/tableView.html'})
                    /* Routes to Add new data view */
                    .when('/new', {
                        controller: 'addCtrl',
                        templateUrl: 'app/dashboard/addDashboard/addView.html'})
                    /*  Routes which shows order View */
                    .when('/info/:custId', {
                        controller: 'infoController',
                        templateUrl: 'app/dashboard/CEOInfoDashboard/infoView.html'})
                     .when('/detail', {
                        controller: 'allCEOController',
                        templateUrl: 'app/dashboard/allCEODashboard/allCEOView.html'})
                    .otherwise({
                        redirectTo: '/'});
        }]);
    window.app = app;
}(window));
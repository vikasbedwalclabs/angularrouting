/*Using Express module */
var express = require('express');
app = express();
app.set('json spaces', 1);
//app.configure(function(){
app.use(express.static(__dirname, '/'));
// }); 
/* Fetching data from local host */
app.get('/customers/:id', function(req, res) {
    var custId = (req.params.id);
    var data = {};

    for (var i = 0, len = customers.length; i < len; i++) {
        if (customers[i].id === custId) {
            data = customers[i];
            break;
        }
    }
    res.type('json');
    res.jsonp(data);
});
/* Fetching data from local host */
app.get('/customers', function(req, res) {
    res.type('json');
    res.jsonp(customers);
    //res.json(500,{eroor:'An error has occured:'});
});
/* Directing data from local host to detail array */
app.get('/detail', function(req, res) {
    detail = [];
    for (var i = 0, len = customers.length; i < len; i++) {
        if (customers[i].detail) {
            console.log(customers[i]);

            for (var j = 0, dlen = customers[i].detail.length; j < dlen; j++) {
                console.log(customers[i].detail.length);
                detail.push(customers[i].detail[j]);
            }
        }
    }
    res.type('json');
    res.jsonp(detail);
});
/* Deleting data from customer array resides into customerController */
app.delete('/customers/:id', function(req, res) {
    var custId = (req.params.id);
    var data = {status: true};
    for (var i = 0, len = customers.length; i < len; i++) {
        if (customers[i].id === custId) {
            customers.splice(i, 1);
            data = {status: true};
            break;
        }
    }
    res.type('json');
    res.jsonp(data);
});
/* After defining below line you have to use 8080 port to view your data from server instead of default  */
app.listen(8080);
console.log('express listening on port 8080');
/* Customer Array */
var customers = [
    {id: '1', company: 'Click Lab', ceo: 'Samar Singla', headquarters: 'California', noe: 200,
        detail: [{exp: '4', nationality: 'India', age: 30,income:1000}]},
    {id: '2', company: 'TCS', ceo: 'Natrajan Chandersekaran', headquarters: 'Mumbai', noe: 349600,
        detail: [{exp: '6', nationality: 'India', age: 52,income:3000}]},
    {id: '3', company: 'Infosys', ceo: 'Vishal Sikka', headquarters: 'Banglore', noe: 255005,
        detail: [{exp: '2', nationality: 'India', age: 48,income:8000}]},
    {id: '4', company: 'Wipro', ceo: 'Azim Premji', headquarters: 'Banglore', noe: 154297,
        detail: [{exp: '14', nationality: 'India', age: 69,income:7000}]},
    {id: '5', company: 'Accenture', ceo: 'Pierre Nanterme', headquarters: 'Dublin Ireland', noe: 319000,
        detail: [{exp: '5', nationality: 'French', age: 56,income:7000}]},
    {id: '6', company: 'Tech Mahindra', ceo: 'Ramalinga Raju', headquarters: 'Pune', noe: 10000,
        detail: [{exp: '6', nationality: 'India', age: 60,income:6000}]},
    {id: '7', company: 'Mphasis', ceo: 'Balu Ganesh Ayyar', headquarters: 'Banglore', noe: 45426,
        detail: [{exp: '10', nationality: 'India', age: 30,income:8000}]},
    {id: '8', company: 'Cisco', ceo: 'John Chamber', headquarters: 'California', noe: 40000,
        detail: [{exp: '9', nationality: 'USA', age: 65,income:3000}]},
    {id: '9', company: 'Oracle', ceo: 'Safra Catz', headquarters: 'California', noe: 122458,
        detail: [{exp: '4', nationality: 'Israel', age: 53,income:5400}]},
    {id: '10', company: 'SAP', ceo: 'Bill McDermott', headquarters: 'Germany', noe: 74400,
        detail: [{exp: '5', nationality: 'US', age: 53,income:4000}]},
    {id: '11', company: 'HCL', ceo: 'Anant Gupta', headquarters: 'Noida', noe: 100000,
        detail: [{exp: '3', nationality: 'India', age: 50,income:3000}]},
    {id: '12', company: 'Google', ceo: 'Larry Page', headquarters: 'California', noe: 53600,
        detail: [{exp: '10', nationality: 'US', age: 41,income:3000}]},
    {id: '13', company: 'Facebook', ceo: 'Mark  Zuckerberg', headquarters: 'California', noe: 8348,
        detail: [{exp: '14', nationality: 'US', age: 30,income:2000}]}];